
import React from 'react';
import { withKnobs, number } from '@storybook/addon-knobs';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {PaginationContainer} from "../../components";


const paginationStories = storiesOf('Pagination', module);

paginationStories.addDecorator(withKnobs);

const lastPage = 5;
paginationStories.add('Default pagination container', () =>
        <PaginationContainer
            currentPage={number('Current page: ', 1,  {
                    range: true,
                    min: 1,
                    max: lastPage,
                    step: 1,
            })}
            currentItemsPerPage={10}
            lastPage={lastPage}
            moveToPage={action('Move to page')}
            itemsPerPageChanged={action('Amount of items per page changed')}
            availableItemsPerPageSelections={[10, 25, 50]}
        />);
