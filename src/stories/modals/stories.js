import React from 'react';
import {text, withKnobs} from '@storybook/addon-knobs';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {AreYouSureModal, ErrorModal, Modal} from "../../components";


const modalsStories = storiesOf('Modals', module);

const okButton = {
  isSubmit: true,
  onClick: action('clicked'),
  text: 'Ok'
};
modalsStories.addDecorator(withKnobs);
const defaultModalText = ' Default text message for the modal panel.'
const content = <div>{text('Text', defaultModalText)}</div>;
const defaultErrorTitle = 'Error Title';
const defaultErrorText = 'Error By doing something';
const defaultErrorMessage = 'We have tried to do something, but we got an error :-(';
const defaultAySTitle = 'AreYouSure Title';
const defaultAySText = 'You have said we should do something.';
modalsStories.add('Default modal panel', () =>
    <Modal
        show={true}
        buttons={[okButton]}
    >
      {content}
    </Modal>
)
    .add('ErrorModal', () =>
        <ErrorModal closeModal={action('Close modal')}
                    title={text('Title', defaultErrorTitle)}
                    text={text('Title', defaultErrorText)}
                    message={text('Title', defaultErrorMessage)}
                    show={true}/>)
    .add('AreYouSureModal', () =>
        <AreYouSureModal closeModal={action('Close modal')}
                         executeAction={action('execute action')}
                         commandId={'theCommand'}
                         data={{id: 4711, name: 'some data'}}
                         title={text('Title', defaultAySTitle)}
                         text={text('Title', defaultAySText)}
                         show={true}/>);
