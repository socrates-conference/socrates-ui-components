import React from 'react';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {boolean, object, withKnobs} from '@storybook/addon-knobs';

import fontawesome from '@fortawesome/fontawesome';
import brands from '@fortawesome/fontawesome-free-brands';
import solid from '@fortawesome/fontawesome-free-solid';
import {Table, EmailCellTemplate, ArrayCellTemplate} from "../../components";

fontawesome.library.add(brands, solid);

const easyColumns = [
  {
    header: 'First name',
    field: 'firstName',
    isFilterable: true,
    visible: true
  },
  {
    header: 'Last name',
    field: 'lastName',
    isFilterable: true,
    visible: true
  },
  {
    header: 'Another field',
    field: 'foo',
    visible: true
  },
];

const easyData = [
  {firstName: 'Maxima', lastName: 'Musterfrau', foo: 'foo data'},
  {firstName: 'Alexander', lastName: 'Musterlastname', foo: 'foo data'},
  {firstName: 'Kinky', lastName: 'Maybelove', foo: 'foo data'},
  {firstName: 'Marg', lastName: 'Fluffy', foo: 'foo data'},
  {firstName: 'Augustus', lastName: 'Ironheart', foo: 'foo data'},
  {firstName: 'Helena', lastName: 'Musterfrau', foo: 'foo data'},
  {firstName: 'Piotr', lastName: 'Musterlastname', foo: 'foo data'},
  {firstName: 'Mumbu', lastName: 'Maybelove', foo: 'foo data'},
  {firstName: 'Krassi', lastName: 'Fluffy', foo: 'foo data'},
  {firstName: 'Dufte', lastName: 'Ironheart', foo: 'foo data'},
  {firstName: 'Bariton', lastName: 'Musterfrau', foo: 'foo data'},
  {firstName: 'Frank', lastName: 'Musterlastname', foo: 'foo data'},
  {firstName: 'Boots', lastName: 'Maybelove', foo: 'foo data'},
  {firstName: 'Frind', lastName: 'Fluffy', foo: 'foo data'},
  {firstName: 'Girasol', lastName: 'Ironheart', foo: 'foo data'}
];

const complexColumns = [
  {
    header: 'First name',
    field: 'firstName',
    isFilterable: true,
    visible: true,
    displayClass: 'bg-secondary text-white'

  },
  {
    header: 'Last name',
    field: 'lastName',
    isFilterable: true,
    visible: true,
    displayClass: 'bg-secondary text-white'
  },
  {
    header: 'Email',
    field: 'email',
    isFilterable: true,
    visible: true,
    displayClass: 'bg-light text-dark',
    cellTemplate: (email) => <EmailCellTemplate value={email}/>

  },
  {
    header: 'Badges',
    field: 'badges',
    isFilterable: true,
    visible: true,
    displayClass: 'bg-light text-primary',
    cellTemplate: (badges) => <ArrayCellTemplate values={badges}/>,
    filter: (rows, field, value) => (rows.filter(x => x.badges.filter(y => y.indexOf(value) >= 0).length > 0)),
  },
];

const complexData = [
  {firstName: 'Maxima', lastName: 'Musterfrau', email: 'maxima@musterfrau.de', badges: ['Primus', 'Maximus']},
  {
    firstName: 'Alexander',
    lastName: 'Musterlastname',
    email: 'alex@musterlastname.co.uk',
    badges: ['Primus', 'Maximus']
  },
  {firstName: 'Kinky', lastName: 'Maybelove', email: 'kynky.maybelove@someprovider.net', badges: ['Primus', 'Maximus']},
  {firstName: 'Marg', lastName: 'Fluffy', email: 'marg.fluffy@another-provider.org', badges: ['Primus', 'Maximus']},
  {firstName: 'Augustus', lastName: 'Ironheart', email: 'a.ironheart@company.com', badges: ['Primus', 'Maximus']},
  {firstName: 'Helena', lastName: 'Musterfrau', email: 'helena@me.org', badges: ['Maximus']},
  {firstName: 'Piotr', lastName: 'Musterlastname', email: 'ptr@musterlastname.co.uk', badges: ['Primus', 'Maximus']},
  {firstName: 'Mumbu', lastName: 'Maybelove', email: 'mumbu.maybelove@someprovider.net', badges: ['Maximus']},
  {firstName: 'Krassi', lastName: 'Fluffy', email: 'krassi@fluffy.es', badges: ['Maximus']},
  {firstName: 'Dufte', lastName: 'Ironheart', email: 'ironheart@dufte.de', badges: []},
  {firstName: 'Bariton', lastName: 'Musterfrau', email: 'bar.mus@gaga.com', badges: ['Primus', 'Maximus']},
  {firstName: 'Frank', lastName: 'Musterlastname', email: 'frank@musterlastname.co.uk', badges: ['Primus', 'Maximus']},
  {firstName: 'Boots', lastName: 'Maybelove', email: 'boots@maybelove.com', badges: ['Primus', 'Maximus']},
  {firstName: 'Frind', lastName: 'Fluffy', email: 'frind@fluffy.de', badges: ['Maximus']},
  {firstName: 'Girasol', lastName: 'Ironheart', email: 'gira.sol@another-provider.org', badges: ['Primus']}
];

const refreshAction = {
  canExecute: () => true,
  execute: action('execute refresh'),
  icon: 'sync',
  buttonClass: 'btn btn-sm btn-success',
  text: 'refresh',
  visible: true
};

const editAction = {
  canExecute: () => true,
  execute: action('execute row edit'),
  icon: 'edit',
  buttonClass: 'btn btn-sm btn-info',
  text: '',
  visible: true
};

const deleteAction = {
  canExecute: () => true,
  execute: action('execute row delete'),
  icon: 'trash',
  buttonClass: 'btn btn-sm btn-danger',
  text: '',
  visible: true
};


const tableStories = storiesOf('Table', module);
tableStories.addDecorator(withKnobs);

tableStories.add('Easy table configuration ', () =>
    <Table columns={easyColumns} data={easyData}
           pagination={{isActive: false}}
    />)
    .add('Complex table configuration', () =>
        <Table
            columns={object('Columns', complexColumns)} data={object('data', complexData)}
            rowActions={[editAction, deleteAction]}
            tableActions={[refreshAction]}
            pagination={
              {
                isActive: boolean('Pagination', true),
                currentItemsPerPage: 10,
                availableItemsPerPageSelections: [10, 25, 50]
              }
            }
        />);
