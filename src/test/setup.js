// @flow
import 'raf/polyfill';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new Adapter()});

// hack to make modules using exenv (such as react-modal) render correctly.
global.window = {document: {createElement: () => {}}, addEventListener: () => {}};
