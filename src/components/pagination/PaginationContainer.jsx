// @flow

import React from 'react';
import * as PropTypes from 'prop-types';
import PaginationItemsPerPageSelector from './PaginationItemsPerPageSelector';
import Paginator from './Paginator';
import './styles.css';

export type PaginationProps = {
  currentPage: number,
  currentItemsPerPage: number,
  lastPage: number,
  moveToPage: (number) => void,
  itemsPerPageChanged: (number) => void,
  availableItemsPerPageSelections: Array<number>
}

export default function PaginationContainer(props: PaginationProps) {
  PaginationContainer.propTypes = {
    availableItemsPerPageSelections: PropTypes.array.isRequired,
    currentItemsPerPage: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    itemsPerPageChanged: PropTypes.func.isRequired,
    lastPage: PropTypes.number.isRequired,
    moveToPage: PropTypes.func.isRequired
  };

  return (
    <div id="pagination" className="container">
      <PaginationItemsPerPageSelector
        currentItemsPerPage={props.currentItemsPerPage}
        itemsPerPageChanged={props.itemsPerPageChanged}
        availableItemsPerPageSelections={props.availableItemsPerPageSelections}/>
      {props.lastPage > 1 && <Paginator
        currentPage={props.currentPage}
        currentItemsPerPage={props.currentItemsPerPage}
        lastPage={props.lastPage}
        moveToPage={props.moveToPage}/>}
    </div>
  );
}
