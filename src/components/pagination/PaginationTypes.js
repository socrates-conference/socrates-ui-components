// @flow

export type PaginationOptions = {
  isActive: boolean,
  currentItemsPerPage: number,
  availableItemsPerPageSelections: Array<number>,
}


