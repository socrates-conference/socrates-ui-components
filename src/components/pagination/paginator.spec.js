import React from 'react';
import {shallow} from 'enzyme';
import Paginator from './Paginator';

describe('(Component) Paginator', () => {
  let mockMoveTo;
  let wrapper;
  beforeEach(() => {
    mockMoveTo = jest.fn();
    wrapper = shallow(
      <Paginator
        currentPage={7}
        currentItemsPerPage={10}
        lastPage={15}
        moveToPage={mockMoveTo}/>
    );
  });

  afterEach(() => {
    mockMoveTo.mockReset();
  });

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('first moves to first page', () => {
    wrapper.find('#first').simulate('click');
    expect(mockMoveTo.mock.calls.length).toBe(1);
    expect(mockMoveTo.mock.calls[0][0]).toBe(1);
  });
  it('previous moves to previous page', () => {
    wrapper.find('#previous').simulate('click');
    expect(mockMoveTo.mock.calls.length).toBe(1);
    expect(mockMoveTo.mock.calls[0][0]).toBe(6);
  });
  it('next moves to next page', () => {
    wrapper.find('#next').simulate('click');
    expect(mockMoveTo.mock.calls.length).toBe(1);
    expect(mockMoveTo.mock.calls[0][0]).toBe(8);
  });
  it('last moves to last page', () => {
    wrapper.find('#last').simulate('click');
    expect(mockMoveTo.mock.calls.length).toBe(1);
    expect(mockMoveTo.mock.calls[0][0]).toBe(15);
  });
});
