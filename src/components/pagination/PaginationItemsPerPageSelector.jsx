// @flow

import React from 'react';
import * as PropTypes from 'prop-types';

type Props = {
  currentItemsPerPage: number,
  itemsPerPageChanged: (number) => void,
  availableItemsPerPageSelections: Array<number>
}

type State = {
  isOpen: boolean
}

export default class PaginationItemsPerPageSelector extends React.Component<Props, State> {
  static propTypes = {
    availableItemsPerPageSelections: PropTypes.array.isRequired,
    currentItemsPerPage: PropTypes.number.isRequired,
    itemsPerPageChanged: PropTypes.func.isRequired
  };

  state: State = {
    isOpen: false
  };

  _toggle = () => {
    this.setState({isOpen: !this.state.isOpen});
  };

  _selected = (length: number) => {
    this.setState({isOpen: false});
    this.props.itemsPerPageChanged(length);
  };

  render = () => {
    return (
      <div className="row justify-content-center text-center mb-2">
        <div id="itemsPerPageDropdown" className="dropdown">
          <button
            className="btn btn-sm btn-secondary dropdown" type="button"
            id="paginationLengthButton" onClick={this._toggle}>
            Page length: {this.props.currentItemsPerPage}
          </button>
          {
            this.state.isOpen ?
              <div className="dropdown-menu show">
                {this.props.availableItemsPerPageSelections.map((length) =>
                  <button
                    id={'length-' + length}
                    key={'length-' + length} className="dropdown-item"
                    onClick={() => this._selected(length)}>{length}</button>)
                }
              </div> : null
          }
        </div>
      </div>
    );
  };
}
