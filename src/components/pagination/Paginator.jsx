// @flow

import React from 'react';
import * as PropTypes from 'prop-types';

export type PaginatorProps = {
  currentPage: number,
  currentItemsPerPage: number,
  lastPage: number,
  moveToPage: (number) => void,
}

export default function Paginator(props: PaginatorProps) {
  Paginator.propTypes = {
    currentItemsPerPage: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    lastPage: PropTypes.number.isRequired,
    moveToPage: PropTypes.func.isRequired
  };
  const previousDisabled = (): boolean => {
    return props.currentPage <= 1;
  };

  const nextDisabled = (): boolean => {
    return props.currentPage >= props.lastPage;
  };

  return(
    <div className="row justify-content-center text-center">
      <nav aria-label="Page navigation example">
        <ul className="pagination">
          <li className="page-item">
            <button id="first"
              className="page-btn-link" onClick={() => props.moveToPage(1)} disabled={previousDisabled()}>
              &lt;&lt;
            </button>
          </li>
          <li className="page-item">
            <button id="previous"
              className="page-btn-link" onClick={() => props.moveToPage(props.currentPage - 1)}
              disabled={previousDisabled()}
            >
              &lt;
            </button>
          </li>
          <li className="page-item">
            <button className="page-text-field" disabled>Page {props.currentPage}</button>
          </li>
          <li className="page-item">
            <button id="next"
              className="page-btn-link" onClick={() => props.moveToPage(props.currentPage + 1)}
              disabled={nextDisabled()}
            >
              &gt;
            </button>
          </li>
          <li className="page-item">
            <button id="last"
              className="page-btn-link" onClick={() => props.moveToPage(props.lastPage)} disabled={nextDisabled()}>
              &gt;&gt;
            </button>
          </li>
        </ul>
      </nav>
    </div>
  );
}

