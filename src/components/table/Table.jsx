// @flow

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TableHead from './head/tableHead';
import TableBody from './body/tableBody';
import PaginationContainer from '../pagination';
import type {ColumnDefinition, Action, TableFilterItem} from './TableTypes';
import type {PaginationOptions} from '../pagination/PaginationTypes';
import TableActionsContainer from './actions/tableActionsContainer';

type State = {
  currentPage: number,
  userDefinedItemsPerPage: number,
  sortedBy: string,
  sortDirection: string,
  filter: Array<TableFilterItem>
}

export type TableProps = {
  columns: Array<ColumnDefinition>,
  data: Array<Object>,
  rowActions: Array<Action>,
  tableActions: Array<Action>,
  pagination: PaginationOptions
}

export default class Table extends Component<TableProps, State> {

  static propTypes = {
    columns: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired,
    pagination: PropTypes.object,
    rowActions: PropTypes.array,
    tableActions: PropTypes.array
  };

  static defaultProps = {
    rowActions: [],
    tableActions: [],
    pagination: {isActive: true, currentItemsPerPage: 10, availableItemsPerPageSelections: [10, 25, 50]},
  };

  state: State = {
    currentPage: 1,
    userDefinedItemsPerPage: 0,
    sortedBy: '',
    sortDirection: '',
    filter: []
  };


  _moveToPage = (newPage: number): void => {
    this.setState({currentPage: newPage});
  };

  _changeItemsPerPage = (itemsPerPage: number): void => {
    const lastPage = Math.ceil(this.props.data.length / itemsPerPage);
    const page = (lastPage < this.state.currentPage) ? lastPage : this.state.currentPage;
    this.setState({userDefinedItemsPerPage: itemsPerPage, currentPage: page});
  };

  _getItemsPerPage = (): number => {
    return this.state.userDefinedItemsPerPage > 0
      ? this.state.userDefinedItemsPerPage
      : this.props.pagination.currentItemsPerPage;
  };

  _dataToShow = (): { rowsToShow: Array<Object>, totalNumberOfRows: number } => {
    if (this.props.data && this.props.data.length > 0) {
      const allRows = this.props.data.slice(0, this.props.data.length);
      const filtered = this._filter(allRows);
      this._sort(filtered);
      const rowsToShow = this._paginate(filtered);
      return {rowsToShow, totalNumberOfRows: filtered.length};
    }
    return {rowsToShow: [], totalNumberOfRows: 0};
  };

  _paginate(rowsToShow: Array<Object>): Array<Object> {
    if (this.props.pagination.isActive) {
      const itemsPerPage = this._getItemsPerPage();
      const pageNumber = this.state.currentPage > 0 ? this.state.currentPage - 1 : 0;
      const initialIndex = pageNumber * itemsPerPage;
      rowsToShow = rowsToShow.slice(initialIndex, initialIndex + itemsPerPage);
    }
    return rowsToShow;
  }

  _sort(rowsToShow: Array<Object>) {
    if (this.state.sortedBy !== '') {
      const column = this.props.columns.find(c => c.header === this.state.sortedBy);
      if (column) {
        const sortFunction = column.sort ? column.sort : this._defaultSort;
        sortFunction(rowsToShow, column.field, this.state.sortDirection);
      }
    }
  }

  _filter(currentRows: Array<Object>): Array<Object> {
    if (this.state.filter.length > 0) {
      let filteredData = currentRows;
      this.state.filter.forEach((currentFilter) => {
        const column = this.props.columns.find((c) => c.header === currentFilter.columnName);
        if (column) {
          const filterFunction = column.filter ? column.filter : this._defaultFilter;
          filteredData = filterFunction(filteredData, column.field, currentFilter.value);
        }
      });
      return filteredData || [];
    }
    return currentRows;
  }

  _defaultSort = (array: Array<Object>, fieldName: string, direction: string) => {
    const ascendant = (a, b) => {
      const fieldA = a[fieldName].toString().toLowerCase();
      const fieldB = b[fieldName].toString().toLowerCase();
      return fieldA.localeCompare(fieldB);
    };
    const descendant = (a, b) => {
      return -ascendant(a, b);
    };
    const getSorterFunctionFor = (theDirection: string) => {
      return theDirection === 'asc' ? ascendant : descendant;
    };

    if (array) {
      array.sort(getSorterFunctionFor(direction));
    }
  };

  _onSort = (columnName: string, direction: string) => {
    this.setState({sortedBy: columnName, sortDirection: direction});
  };

  _defaultFilter = (allData: Array<Object>, field: string, value: string): Array<Object> => {
    return allData.filter((item) => item[field].toLowerCase().indexOf(value.toLowerCase()) >= 0);
  };

  _onFilter = (filter: Array<TableFilterItem>) => {
    this.setState({filter, currentPage: 1});
  };

  render = () => {
    const {rowsToShow, totalNumberOfRows} = this._dataToShow();
    const itemsPerPage = this._getItemsPerPage();
    const hasActions = this.props.rowActions && this.props.rowActions.length > 0;
    const shouldShowPagination = this.props.pagination.isActive;
    const lastPage = Math.ceil(totalNumberOfRows / itemsPerPage);
    return (
      <div>
        {
          this.props.tableActions && this.props.tableActions.length > 0 ?
            <TableActionsContainer actions={this.props.tableActions} allListItems={this.props.data}/> : null
        }
        <div className="table-responsive">
          <table className="table table-bordered table-striped table-hover table-sm">
            <TableHead
              columns={this.props.columns}
              hasActions={hasActions}
              onSort={this._onSort}
              onFilter={this._onFilter}
            />
            <TableBody
              columns={this.props.columns}
              data={rowsToShow}
              rowActions={this.props.rowActions}/>
          </table>
        </div>
        {
          shouldShowPagination
            ? <PaginationContainer
              currentPage={this.state.currentPage}
              currentItemsPerPage={itemsPerPage}
              lastPage={lastPage}
              moveToPage={this._moveToPage}
              itemsPerPageChanged={this._changeItemsPerPage}
              availableItemsPerPageSelections={this.props.pagination.availableItemsPerPageSelections}
            />
            : null
        }
      </div>
    );
  };
}

