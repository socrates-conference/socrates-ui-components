import {executeActionIfAllowed} from './tableActionContainerHelper';

describe('tableActionContainerHelper', () => {
  it('executes action when canExecute is true', () => {
    const action = {canExecute: () => true, execute: jest.fn()};
    const items = [];
    executeActionIfAllowed(action, items);
    expect(action.execute.mock.calls).toHaveLength(1);
  });
  it('does not execute action when canExecute is false', () => {
    const action = {canExecute: () => false, execute: jest.fn()};
    const items = [];
    executeActionIfAllowed(action, items);
    expect(action.execute.mock.calls).toHaveLength(0);
  });
});