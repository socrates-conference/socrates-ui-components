import React from 'react';
import {shallow} from 'enzyme';
import TableActionsContainer from '.';

describe('(Component) TableActionsContainer', () => {
	describe('renders with actions', () => {
		let executeMock;
		beforeEach(() => {
			executeMock = jest.fn();
		});
		const wrapper = shallow(
			<TableActionsContainer actions={[{
				execute: () => executeMock(), canExecute: () => true, icon: {}, buttonClass: '',
				text: '', visible: true
			}]} allListItems={[]}/>
		);

		it('renders without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});

		it('renders correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
		it('can click button', () => {
			wrapper.find('#action-button-0').simulate('click');
			expect(executeMock.mock.calls).toHaveLength(1);
		});
	});
	describe('renders without actions', () => {
		const wrapper = shallow(
			<TableActionsContainer actions={[]} allListItems={[]}/>
		);

		it('renders without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});

		it('renders correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
	});
});
