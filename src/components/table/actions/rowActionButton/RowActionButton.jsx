// @flow

import React from 'react';
import type {Action} from '../../TableTypes';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import PropTypes from "prop-types";

export type TableActionButtonProps = {
  action: Action,
  data: Object
}

export default function RowActionButton(props: TableActionButtonProps) {
  const buttonClass = 'btn btn-sm ' + props.action.buttonClass;
  const executeActionIfAllowed = () => {
    if (props.action.canExecute(props.data)) {
      props.action.execute(props.data);
    }
  };
  return (
    <button
      className={buttonClass}
      onClick={executeActionIfAllowed}
      disabled={!props.action.canExecute(props.data)}>
      <FontAwesomeIcon icon={props.action.icon} />
      <span className="sr-only">{props.action.text}</span>
    </button>
  );
}

RowActionButton.propTypes = {
	action: PropTypes.object.isRequired,
	data: PropTypes.object.isRequired,
};
