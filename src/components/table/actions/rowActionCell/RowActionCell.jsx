// @flow

import React from 'react';
import RowActionButton from '../rowActionButton';
import type {Action} from '../../TableTypes';
import PropTypes from "prop-types";

export type TableActionCellProps = {
  rowActions: Array<Action>,
  data: Object
}

export default function RowActionCell(props: TableActionCellProps) {
  const visibleActions =
    (props.rowActions && props.rowActions.length > 0) ? props.rowActions.filter(item => item.visible) : [];


  return (
    <td>
      {
        visibleActions.map((action, index) =>
          <RowActionButton key={'actionButton' + index} action={action} data={props.data}/>)
      }
    </td>
  );
}

RowActionCell.propTypes = {
	rowActions: PropTypes.array.isRequired,
	data: PropTypes.object.isRequired,
};
