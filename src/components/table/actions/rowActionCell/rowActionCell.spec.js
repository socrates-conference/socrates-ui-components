import React from 'react';
import {shallow} from 'enzyme/build';
import RowActionCell from '.';

describe('(Component) RowActionCell', () => {
	describe('renders content when visible actions available', () => {
		const wrapper = shallow(
			<RowActionCell rowActions={[{
				execute: () => {
				}, icon: {}, buttonClass: '', text: '', visible: true
			}]} data={{}}/>
		);

		it('renders without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});

		it('renders correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
	});
	describe('renders empty when visible no actions available', () => {
		const wrapper = shallow(
			<RowActionCell rowActions={[]} data={{}}/>
		);

		it('renders without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});

		it('renders correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
	});
});
