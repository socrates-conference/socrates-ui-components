// @flow

import React from 'react';
import type {Node} from 'react';
import PropTypes from "prop-types";

export type TableCellProps = {
  content: any,
  displayClass?: string,
  format?: (any) => string,
  cellTemplate?: (any) => Node
};


export default function TableCell(props: TableCellProps) {
  const renderContent = () => {
    if(props.cellTemplate) {
      return props.cellTemplate(props.content);
    } else if (props.format) {
      return props.format(props.content);
    } else {
      return props.content;
    }
  };
  return (
    <td className={props.displayClass}>
      {renderContent()}
    </td>
  );
}
TableCell.propTypes = {
	content: PropTypes.any.isRequired,
	displayClass: PropTypes.string,
	format: PropTypes.func,
	cellTemplate: PropTypes.func
};
