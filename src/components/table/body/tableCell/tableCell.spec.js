import React from 'react';
import {shallow, mount} from 'enzyme/build';
import TableCell from '.';

describe('(Component) TableCell', () => {
  describe('renders', () => {
    const wrapper = shallow(
      <TableCell
        key="cell"
        content="content"
        format={() => {
        }}
        displayClass=""
        cellTemplate={() => {
        }}
      />
    );

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  it('uses format function if it is available', () => {
    const mockCallback = jest.fn();
    const wrapper = mount(<table><tbody><tr><TableCell content="content" format={mockCallback}/></tr></tbody></table>);
    expect(wrapper).toBeDefined();
    expect(mockCallback.mock.calls.length).toBe(1);
    expect(mockCallback.mock.calls[0][0]).toEqual('content');
  });
  it('uses cellTemplate if it is available', () => {
    const mockCallback = jest.fn();
    const wrapper = mount(
      <table><tbody><tr><TableCell content="content" cellTemplate={mockCallback}/></tr></tbody></table>
    );
    expect(wrapper).toBeDefined();
    expect(mockCallback.mock.calls.length).toBe(1);
    expect(mockCallback.mock.calls[0][0]).toEqual('content');
  });
  it('uses cellTemplate when cellTemplate and format function available', () => {
    const mockCellTemplate = jest.fn();
    const mockFormat = jest.fn();
    const wrapper = mount(
      <table><tbody><tr>
        <TableCell content="content" format={mockFormat} cellTemplate={mockCellTemplate}/>
      </tr></tbody></table>
    );
    expect(wrapper).toBeDefined();
    expect(mockFormat.mock.calls.length).toBe(0);
    expect(mockCellTemplate.mock.calls.length).toBe(1);
    expect(mockCellTemplate.mock.calls[0][0]).toEqual('content');
  });
  it('uses raw content when cellTemplate and format function are both not available', () => {
    const wrapper = mount(
      <table><tbody><tr>
        <TableCell content="content" displayClass="displayClass"/>
      </tr></tbody></table>
    );
    expect(wrapper.find('td').text()).toContain('content');
  });
  it('uses the given displayClass for the table cell', () => {
    const wrapper = mount(
      <table><tbody><tr>
        <TableCell content="content" displayClass="displayClass"/>
      </tr></tbody></table>
    );
    expect(wrapper.find('td').hasClass('displayClass')).toBe(true);
  });
});
