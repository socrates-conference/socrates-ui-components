// @flow

import React from 'react';
import TableRow from '../tableRow';
import type {ColumnDefinition, Action} from '../../TableTypes';
import PropTypes from "prop-types";

export type TableBodyProps = {
  columns: Array<ColumnDefinition>,
  data: Array<Object>,
  rowActions: Array<Action>
};

export default function TableBody(props: TableBodyProps) {
  return (
    <tbody>
      {
        props.data.map((row, index) =>
          <TableRow key={'row' + index} content={row} columns={props.columns} rowActions={props.rowActions}/>)
      }
    </tbody>
  );
}

TableBody.propTypes = {
	columns: PropTypes.array.isRequired,
	data: PropTypes.array.isRequired,
	rowActions: PropTypes.array.isRequired
};
