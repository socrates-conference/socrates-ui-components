import React from 'react';
import {shallow} from 'enzyme/build';
import TableRow from './TableRow';

describe('(Component) TableRow', () => {
	describe('renders', () => {
		describe('when has visible columns and actions', () => {
			const columns = [{header: 'Name', field: 'name', isSortable: true,
				isFilterable: true, visible: true}];
			const content = {name: 'Name1'};
			const actions = [{
				execute: () => {}, canExecute: () => true, icon: {}, buttonClass: '',
				text: '', visible: true
			}];
			const wrapper = shallow(
				<TableRow rowActions={actions} content={content} columns={columns}/>
			);

			it('renders without exploding', () => {
				expect(wrapper).toHaveLength(1);
			});

			it('renders correctly', () => {
				expect(wrapper).toMatchSnapshot();
			});
		});
		describe('when has no visible columns and no actions', () => {
			const columns = [{header: 'Name', field: 'name', isSortable: true,
				isFilterable: true}];
			const content = {name: 'Name1'};
			const actions = [];
			const wrapper = shallow(
				<TableRow rowActions={actions} content={content} columns={columns}/>
			);

			it('renders without exploding', () => {
				expect(wrapper).toHaveLength(1);
			});

			it('renders correctly', () => {
				expect(wrapper).toMatchSnapshot();
			});
		});
	});
});
