// @flow

import React from 'react';
import RowActionCell from '../../actions/rowActionCell';
import type {ColumnDefinition, Action} from '../../TableTypes';
import PropTypes from "prop-types";
import TableCell from "../tableCell";

export type TableRowProps = {
  columns: Array<ColumnDefinition>,
  content: {},
  rowActions: Array<Action>
};

export default function TableRow(props: TableRowProps) {
  const hasActions = props.rowActions && props.rowActions.length > 0;
  return (
    <tr>
      {
        props.columns.map((column, index) =>
          column.visible ? <TableCell
            key={'cell-' + column.header + '-' + index}
            content={props.content[column.field]}
            format={column.format}
            displayClass={column.displayClass}
            cellTemplate = {column.cellTemplate}
          /> : null)
      }
      {hasActions ? <RowActionCell rowActions={props.rowActions} data={props.content}/> : null}
    </tr>
  );
}

TableRow.propTypes = {
	columns: PropTypes.array.isRequired,
	content: PropTypes.object.isRequired,
	rowActions: PropTypes.array.isRequired
};
