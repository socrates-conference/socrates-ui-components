// @flow

import React from 'react';
import PropTypes from "prop-types";

export default function BooleanCellTemplate(props: { value: boolean }) {
  if (props.value) {
    return <div>
      <div className="text-success">yes</div>
    </div>;
  } else {
    return <div>
      <div className="text-danger">no</div>
    </div>;
  }
}
BooleanCellTemplate.propTypes = {
	value: PropTypes.bool.isRequired
};
