import React from 'react';
import {shallow} from 'enzyme/build';
import BooleanCellTemplate from "./BooleanCellTemplate";

describe('(Component) BooleanCellTemplate', () => {
  describe('with true value', () =>{
    const wrapper = shallow(
        <BooleanCellTemplate value={true}/>
    );

    it('renders without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });
  describe('with false value', () =>{
    const wrapper = shallow(
        <BooleanCellTemplate value={false}/>
    );

    it('renders without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });
});
