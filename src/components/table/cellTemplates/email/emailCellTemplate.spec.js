import React from 'react';
import {shallow} from 'enzyme';
import EmailCellTemplate from './EmailCellTemplate';

describe('(Component) EmailCellTemplate', () => {
  const wrapper = shallow(
    <EmailCellTemplate value="valid@email.de"/>
  );

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
