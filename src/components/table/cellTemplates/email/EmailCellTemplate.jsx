// @flow

import React from 'react';
import PropTypes from "prop-types";

export default function EmailCellTemplate (props: {value: string}) {
  return (
    <div>
      <a href={'mailto:' + props.value}>
        {props.value}
      </a>
    </div>
  );
}

EmailCellTemplate.propTypes = {
	value: PropTypes.string.isRequired
};
