// @flow

import React from 'react';
import PropTypes from "prop-types";
import './styles.css';

type Props = {
	values: Array<Object>
}

export default function ArrayCellTemplate (props: Props) {
  return (
    <div>
      {
        props.values.map((item, index) =>
          <div key={'arrayItem-' + index.toString()} className="room-type-bubble d-md-inline-flex">
            {item.toString()}
          </div>
        )
      }
    </div>
  );
}

ArrayCellTemplate.propTypes = {
	values: PropTypes.array.isRequired
};
