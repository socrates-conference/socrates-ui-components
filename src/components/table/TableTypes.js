// @flow

import type {Node} from 'react';

export type ColumnStatus = { header: string, visible: boolean };
export type ActionStatus = { text: string, visible: boolean };

export type ColumnDefinition = {
  header: string,
  displayClass?: string,
  field: string,
  format?: (any) => string,
  cellTemplate?: (any) => Node,
  isSortable?: boolean,
  isFilterable?: boolean,
  sort?: (Array<Object>, string, string) => void,
  filter?: (Array<Object>, string, string) => Array<Object>,
  visible: boolean
}

export type Action = {
  canExecute: (any) => boolean,
  execute: (any) => void,
  icon: string,
  buttonClass: string,
  text: string,
  visible: boolean
}

export type SortOptions = {
  isSortable: boolean,
  isSortColumn: boolean,
  onSort: (string, string) => void
}

export type TableFilterItem = {
  columnName: string,
  value: string
}

