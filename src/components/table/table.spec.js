import React from 'react';
import {shallow} from 'enzyme';
import Table from './Table';
import data from './tableTestData';

describe('(Component) Table', () => {
  describe('when using default functions', () => {
    const mockCallback = jest.fn();
    const columns = [
      {header: 'Name', field: 'name', isSortable: true, isFilterable: true},
      {header: 'Adr', field: 'address', isSortable: true, isFilterable: true}
    ];
    const actions = [{execute: () => {}, canExecute: true, icon: {}, buttonClass: '', text: ''}];
    let wrapper;

    afterEach(() => {
      mockCallback.mockReset();
    });

    beforeEach(() => {
      wrapper = shallow(
        <Table
          columns={columns}
          data={data}
          actions={actions}
          refresh={mockCallback}
        />
      );

    });
    it('renders without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('renders correctly with Pagination', () => {
      expect(wrapper).toMatchSnapshot();
    });

    it('renders correctly without Pagination', () => {
      wrapper = shallow(
        <Table
          columns={columns}
          data={[]}
          actions={actions}
          refresh={mockCallback}
        />);
      expect(wrapper).toMatchSnapshot();
    });

    it('the initial state is defined', () => {
      expect(wrapper.state('currentPage')).toBe(1);
      expect(wrapper.state('userDefinedItemsPerPage')).toBe(0);
      expect(wrapper.state('sortedBy')).toEqual('');
      expect(wrapper.state('sortDirection')).toEqual('');
      expect(wrapper.state('filter').length).toBe(0);
    });

    it('sets the filter state when the onFilter function is called', () => {
      wrapper.instance()._onFilter([{columnName: 'Name', value: 'Voltaire'}]);
      const filter = wrapper.state('filter');
      expect(filter.length).toBe(1);
      expect(filter[0].columnName).toEqual('Name');
      expect(filter[0].value).toEqual('Voltaire');
    });

    it('sets the sort information state when OnSort is called', () => {
      wrapper.instance()._onSort('Name', 'asc');
      expect(wrapper.state('sortedBy')).toEqual('Name');
      expect(wrapper.state('sortDirection')).toEqual('asc');
    });

    it('sets the current page when moveTo is called', () => {
      wrapper.instance()._moveToPage(5);
      expect(wrapper.state('currentPage')).toBe(5);
    });

    it('has 10 as default number of items per page', () => {
      expect(wrapper.instance()._getItemsPerPage()).toBe(10);
    });

    it('updates number of items per page when changeItemsPerPage is called ', () => {
      wrapper.instance()._changeItemsPerPage(50);
      expect(wrapper.state('userDefinedItemsPerPage')).toBe(50);
      expect(wrapper.instance()._getItemsPerPage()).toBe(50);
    });

    it('updates the current page when after a change in the number of items per page the last possible page is smaller than the current page', () => {
      wrapper.instance()._moveToPage(5);
      wrapper.instance()._changeItemsPerPage(50);
      expect(wrapper.state('currentPage')).toBe(2);
    });

    it('displays as many lines as \'items per page\' says.', () => {
      wrapper.instance()._changeItemsPerPage(25);
      const dataToShow = wrapper.instance()._dataToShow();
      expect(dataToShow.rowsToShow.length).toBe(25);
      expect(dataToShow.totalNumberOfRows).toBe(56);
    });

    it('sorts the data ascendant if the sorting information says so', () => {
      wrapper.instance()._onSort('Name', 'asc');
      const dataToShow = wrapper.instance()._dataToShow();
      expect(dataToShow.rowsToShow[0].name).toBe('Anna Wind');
    });

    it('sorts the data descendant if the sorting information says so', () => {
      wrapper.instance()._onSort('Name', 'desc');
      const dataToShow = wrapper.instance()._dataToShow();
      expect(dataToShow.rowsToShow[0].name).toBe('Yvonne Los');
    });

    it('filters the data according to the default filter criteria using case insensitive search', () => {
      wrapper.instance()._onFilter([{columnName: 'Name', value: 'vOltAire'}]);
      const dataToShow = wrapper.instance()._dataToShow();
      expect(dataToShow.rowsToShow.length).toBe(2);
      expect(dataToShow.totalNumberOfRows).toBe(2);
    });
  });
  describe('when using  external sort and filter functions', () => {
    const mockFilter = jest.fn();
    const mockSort = jest.fn();
    const columns = [
      {header: 'Name', field: 'name', isSortable: true, isFilterable: true, filter: mockFilter, sort: mockSort},
      {header: 'Adr', field: 'address', isSortable: true, isFilterable: true}
    ];
    const actions = [{callback: () => {}, icon: {}, buttonClass: '', text: ''}];
    let wrapper;

    afterEach(() => {
      mockFilter.mockReset();
      mockSort.mockReset();
    });

    beforeEach(() => {
      wrapper = shallow(
        <Table
          columns={columns}
          data={data}
          actions={actions}
          refresh={() => {}}
        />
      );

    });

    it('these functions will be used to determine the rows that will be shown', () => {
      wrapper.instance()._onSort('Name', 'asc');
      wrapper.instance()._onFilter([{columnName: 'Name', value: 'v'}]);
      mockSort.mockReset(); // Reset calls by component initialization an rendering
      mockFilter.mockReset(); // Reset calls by component initialization an rendering
      wrapper.instance()._dataToShow();
      expect(mockFilter.mock.calls.length).toBe(1);
      expect(mockSort.mock.calls.length).toBe(1);
    });
  });
});
