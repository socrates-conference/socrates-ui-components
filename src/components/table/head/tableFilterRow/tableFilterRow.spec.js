import React from 'react';
import {shallow} from 'enzyme/build';
import TableFilterRow from './TableFilterRow';

describe('(Component) TableFilterRow', () => {
  describe('renders with actions', () => {
    const columns = [
      {header: 'Name', field: 'name', isSortable: true, isFilterable: true, visible: true},
      {header: 'Name2', field: 'name2', isSortable: true, isFilterable: false, visible: true},
      {header: 'Name2', field: 'name2', isSortable: true, isFilterable: false, visible: false}
    ];
    const wrapper = shallow(
        <TableFilterRow columns={columns} hasActions={true} onFilter={() => {
        }}/>
    );

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });
  describe('renders without actions', () => {
    const columns = [
      {header: 'Name', field: 'name', isSortable: true, isFilterable: true, visible: true},
      {header: 'Name2', field: 'name2', isSortable: true, isFilterable: false, visible: true},
      {header: 'Name2', field: 'name2', isSortable: true, isFilterable: false, visible: false}
    ];
    const wrapper = shallow(
        <TableFilterRow columns={columns} hasActions={false} onFilter={() => {
        }}/>
    );

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('after receiving onChange event', () => {
    let wrapper;
    const addOrUpdateFilter = (columnName, value) => {
      wrapper.instance()._onChange(columnName, value);
      wrapper.update();
    };
    beforeEach(() => {
      const mockFilter = jest.fn();
      const columns = [{header: 'Name', field: 'name', isSortable: true, isFilterable: true}];
      wrapper = shallow(
          <TableFilterRow columns={columns} hasActions={true} onFilter={mockFilter}/>
      );
    });
    it('starts a new timeout', () => {
      const initialIntervalId = wrapper.state('intervalId');
      addOrUpdateFilter('columnName', 'value');
      expect(wrapper.state('intervalId')).toBeDefined();
      expect(wrapper.state('intervalId') !== initialIntervalId).toBe(true);
    });
    it('add the column and its value to the filter if the column was unfiltered', () => {
      addOrUpdateFilter('columnName', 'value');
      const filter = wrapper.state('filter').find((c) => c.columnName === 'columnName');
      expect(filter).toBeDefined();
      expect(filter.value).toEqual('value');
    });
    it('changes the filter string for the column if the column had already a filter', () => {
      addOrUpdateFilter('columnName', 'value');
      addOrUpdateFilter('columnName', 'anotherValue');
      const filter = wrapper.state('filter').find((c) => c.columnName === 'columnName');
      expect(filter).toBeDefined();
      expect(filter.value).toEqual('anotherValue');
    });
    it('removes a column from the filter list if the filter value is empty', () => {
      addOrUpdateFilter('columnName', 'value');
      addOrUpdateFilter('columnName', '');
      const filter = wrapper.state('filter').find((c) => c.columnName === 'columnName');
      expect(filter).toBeUndefined();
    });
    it('removes a column from the filter list if the trimmed filter value is empty', () => {
      addOrUpdateFilter('columnName', 'value');
      addOrUpdateFilter('columnName', '      ');
      const filter = wrapper.state('filter').find((c) => c.columnName === 'columnName');
      expect(filter).toBeUndefined();
    });
    it('refuses to add a column to the filter list when the trimmed filter value is empty', () => {
      addOrUpdateFilter('columnName', '      ');
      const filter = wrapper.state('filter').find((c) => c.columnName === 'columnName');
      expect(filter).toBeUndefined();
    });
  });
  describe('calls onFilter', () => {
    let wrapper, mockFilter;
    beforeEach(() => {
      mockFilter = jest.fn();
      const columns = [{header: 'Name', field: 'name', isSortable: true, isFilterable: true}];
      wrapper = shallow(
          <TableFilterRow columns={columns} hasActions={true} onFilter={mockFilter}/>
      );
    });
    it('after filter changes', () => {
      wrapper.instance()._filter();
      expect(mockFilter.mock.calls).toHaveLength(1)
    });
  });
});
