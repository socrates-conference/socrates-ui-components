import React from 'react';
import {shallow} from 'enzyme/build';
import TableHead from './TableHead';

describe('(Component) RowActionCell', () => {
	describe('renders with visible columns', () => {
		const columns = [
			{header: 'Name', field: 'name', isSortable: false, isFilterable: true,
				visible: true},
			{header: 'Adr', field: 'address', isSortable: true, isFilterable: true,
				visible: true}
		];
		const wrapper = shallow(
			<TableHead
				columns={columns}
				hasActions={true}
				onSort={() => {}}
				onFilter={() => {}}
			/>
		);

		it('renders without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});

		it('renders correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
	});
	describe('renders without visible columns', () => {
		const columns = [
			{header: 'Name', field: 'name', isSortable: true, isFilterable: false,
				visible: false},
			{header: 'Adr', field: 'address', isSortable: true, isFilterable: false,
				visible: false}
		];
		const wrapper = shallow(
			<TableHead
				columns={columns}
				hasActions={false}
				onSort={() => {}}
				onFilter={() => {}}
			/>
		);

		it('renders without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});

		it('renders correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
	});
});
