import React from 'react';
import {shallow} from 'enzyme/build';
import TableFilterCell from './TableFilterCell';

describe('(Component) TableFilterCell', () => {
  describe('renders', () => {
    const wrapper = shallow(
      <TableFilterCell columnName="columnName" displayClass="displayClass" onChange={() => {}}/>
    );

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  it('calls change function if filter input changes', () => {
    const mockCallback = jest.fn();
    const wrapper = shallow(
      <TableFilterCell columnName="columnName" displayClass="displayClass" onChange={mockCallback}/>
    );
    wrapper.find('input').simulate('change', {currentTarget: {value: 'changedText'}});
    expect(mockCallback.mock.calls.length).toBe(1);
    expect(mockCallback.mock.calls[0][0]).toEqual('columnName');
    expect(mockCallback.mock.calls[0][1]).toEqual('changedText');
  });
  it('sets filter value into state if filter input changes', () => {
    const mockCallback = jest.fn();
    const wrapper = shallow(
      <TableFilterCell columnName="columnName" displayClass="displayClass" onChange={mockCallback}/>
    );
    wrapper.find('input').simulate('change', {currentTarget: {value: 'changedText'}});
    expect(wrapper.state('value')).toEqual('changedText');
  });
});
