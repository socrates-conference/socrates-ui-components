import React from 'react';
import {shallow} from 'enzyme/build';
import TableHeaderRow from './TableHeaderRow';

describe('(Component) TableHeaderRow', () => {
	describe('renders with filterable columns', () => {
		const columns = [{header: 'Name', field: 'name', isSortable: true,
			isFilterable: true, visible: true}];
		const wrapper = shallow(
			<TableHeaderRow columns={columns} onSort={() => {
			}} hasActions={true}/>
		);

		it('without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});

		it('correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
	});

	describe('renders without visible columns', () => {
		const columns = [{header: 'Name', field: 'name', isSortable: true,
			isFilterable: false, visible: false}];
		const wrapper = shallow(
			<TableHeaderRow columns={columns} onSort={() => {
			}} hasActions={true}/>
		);

		it('without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});

		it('correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
	});

	it('updates sorting column on child cell click', () => {
    const mockCallback = jest.fn();
    const columns = [{header: 'Name', field: 'name', isSortable: true,
			isFilterable: true, visible: true}];
    const wrapper = shallow(
      <TableHeaderRow columns={columns} onSort={mockCallback} hasActions={true}/>
    );
    wrapper.instance()._onClick('columnName');
    expect(wrapper.state('sortingColumn')).toEqual('columnName');
  });
});
