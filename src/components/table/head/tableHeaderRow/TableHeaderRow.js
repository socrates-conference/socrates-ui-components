// @flow

import React, {Component} from 'react';
import TableHeaderCell from '../tableHeaderCell';
import PropTypes from 'prop-types';
import type {ColumnDefinition} from '../../TableTypes';

type TableHeaderRowState = {
  sortingColumn: string
}

export type TableHeaderRowProps = {
  columns: Array<ColumnDefinition>,
  hasActions: boolean,
  onSort: (string, string) => void

}

export default class TableHeaderRow extends Component<TableHeaderRowProps, TableHeaderRowState> {
  static propTypes = {
    columns: PropTypes.array.isRequired,
    hasActions: PropTypes.bool.isRequired,
    onSort: PropTypes.func
  };

  state: TableHeaderRowState = {
    sortingColumn: ''
  };

  _onClick = (columnName: string) => {
    this.setState({sortingColumn: columnName});
  };

  render = () => {
    return (
      <tr>
        {this.props.columns.map((column, index) =>
          column.visible
            ? <TableHeaderCell
              key={'header' + index}
              display={column.header}
              displayClass={column.displayClass}
              sortOptions={
                {
                  isSortable: column.isSortable ? column.isSortable : true,
                  isSortColumn: this.state.sortingColumn === column.header,
                  onSort: this.props.onSort
                }}
              onClick={this._onClick}
            /> : null)
        }
        {this.props.hasActions ? <TableHeaderCell key="headerActions" display="Actions"/> : null}
      </tr>
    );
  };
}

