import React from 'react';
import {shallow} from 'enzyme/build';
import TableHeaderCell from './TableHeaderCell';

describe('(Component) TableHeaderCell', () => {
  describe('renders', () => {
    const wrapper = shallow(
      <TableHeaderCell
        columnName="columnName" displayClass="displayClass"
        onClick={() => {
        }} display="columnName"
        sortOptions={
          {
            isSortable: true,
            isSortColumn: true,
            onSort: () => {
            }
          }}
      />
    );

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  it('reacts to header clicks', () => {
    const mockCallback = jest.fn();
    const wrapper = shallow(
      <TableHeaderCell
        columnName="columnName" displayClass="displayClass"
        onClick={mockCallback} display="columnName"
        sortOptions={
          {
            isSortable: true,
            isSortColumn: true,
            onSort: () => {
            }
          }}
      />
    );
    wrapper.find('th').simulate('click');
    expect(mockCallback.mock.calls.length).toBe(1);
    expect(mockCallback.mock.calls[0][0]).toEqual('columnName');
  });
  it('forwards the name of th column, whe it is clicked', () => {
    const mockCallback = jest.fn();
    const wrapper = shallow(
      <TableHeaderCell
        columnName="columnName" displayClass="displayClass"
        onClick={() => {
        }} display="columnName"
        sortOptions={
          {
            isSortable: true,
            isSortColumn: true,
            onSort: mockCallback
          }}
      />
    );
    wrapper.find('th').simulate('click');
    expect(mockCallback.mock.calls.length).toBe(1);
    expect(mockCallback.mock.calls[0][0]).toEqual('columnName');
  });
  it('has no sort direction after initialization', () => {
    const wrapper = shallow(
      <TableHeaderCell
        columnName="columnName" displayClass="displayClass"
        onClick={() => {
        }} display="columnName"
        sortOptions={
          {
            isSortable: true,
            isSortColumn: true,
            onSort: () => {
            }
          }}
      />
    );
    expect(wrapper.state('currentSortDirection')).toEqual('none');
  });
  describe('state changes', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(
        <TableHeaderCell
          columnName="columnName" displayClass="displayClass"
          onClick={() => {
          }} display="columnName"
          sortOptions={
            {
              isSortable: true,
              isSortColumn: true,
              onSort: () => {
              }
            }}
        />
      );
    });
    it('to sort ascending if previously was not sorted and the header is clicked', () => {
      expect(wrapper.state('currentSortDirection')).toEqual('none');
      wrapper.find('th').simulate('click');
      expect(wrapper.state('currentSortDirection')).toEqual('asc');
    });
    it('to sort ascending if previously was sorting descendant and the header is clicked', () => {
      wrapper.find('th').simulate('click');
      wrapper.find('th').simulate('click');
      expect(wrapper.state('currentSortDirection')).toEqual('desc');
      wrapper.find('th').simulate('click');
      expect(wrapper.state('currentSortDirection')).toEqual('asc');
    });
    it('to sort descending if previously was sorting ascendant and the header is clicked', () => {
      wrapper.find('th').simulate('click');
      expect(wrapper.state('currentSortDirection')).toEqual('asc');
      wrapper.find('th').simulate('click');
      expect(wrapper.state('currentSortDirection')).toEqual('desc');
    });
  });
})
;
