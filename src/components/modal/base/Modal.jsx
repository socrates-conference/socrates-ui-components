// @flow

import React from 'react';
import * as PropTypes from 'prop-types';
import './styles.css';

export type ModalButton = {
  isSubmit: boolean,
  onClick: () => void,
  text: string
}
type Props = {
  buttons: Array<ModalButton>,
  children: any,
  show: boolean
};

export default function Modal(props: Props) {

  const submit = (event) => {
    event.preventDefault();
    const submitButton = props.buttons.find(button => button.isSubmit);
    if (submitButton) {
      submitButton.onClick();
    }
  };

  const buttons =

    <div className="modal-footer">
      {
        props.buttons.map((button) =>
          <button
            key={`modal-button-${button.text.replace(' ', '')}`} type={button.isSubmit ? 'submit' : 'button'}
            id={`modal-button-${button.text.replace(' ', '')}`}
            className="btn btn-secondary"
            onClick={button.isSubmit ? null : button.onClick}>
            {button.text}
          </button>
        )
      }
    </div>;

  return (
    props.show ?
      <div className="backdrop">
        <div className="modal-block">
          <form className="form" id="application-form" onSubmit={submit}>
            {props.children}
            {buttons}
          </form>
        </div>
      </div> : <div></div>
  );
}

Modal.propTypes = {
  buttons: PropTypes.array.isRequired,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.array]).isRequired,
  show: PropTypes.bool.isRequired

};
