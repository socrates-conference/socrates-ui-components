// @flow

import React from 'react';
import * as PropTypes from 'prop-types';
import type {ModalButton} from "../base/Modal";
import Modal from "../base/Modal";

type Props = {
  closeModal: () => void,
  executeAction: (Object, string) => void,
  commandId: string,
  data: Object,
  title: string,
  text: string,
  show: boolean,
}

export default function AreYouSureModal(props: Props) {

  const buttons: Array<ModalButton> = [
    {
      isSubmit: true, onClick: () => {
        props.executeAction(props.data, props.commandId);
      }, text: 'Ok'
    },
    {
      isSubmit: false, onClick: () => {
        props.closeModal();
      }, text: 'Cancel'
    }
  ];

  return (
    <Modal show={props.show} buttons={buttons}>
      <div className="modal-header">
        <h3 className="mb-2 mx-auto">
          <strong>{props.title}</strong>
        </h3>
      </div>
      <div className="mb-2 mx-auto alert alert-info">
        <small>{props.text}</small>
      </div>
      <div className="form-row">
        <h3>Are you sure?</h3>
      </div>
    </Modal>
  );
}
AreYouSureModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  commandId: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

