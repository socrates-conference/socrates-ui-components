import React from 'react';
import {mount} from 'enzyme';
import AreYouSureModal from '.';
import {spy} from 'sinon';

const data = {};

describe('(Component) AreYouSureModal', () => {
  let wrapper;
  let closeSpy;
  let executeSpy;
	describe('renders content when visible', () => {
		beforeEach(() => {
			closeSpy = spy();
			executeSpy = spy();
			wrapper = mount(
				<AreYouSureModal
					id="areYouSureModal" data={data} commandId="delete"
					show={true}
					closeModal={closeSpy} title="Test title" text="Test text"
					executeAction={executeSpy}
				/>);
		});

		it('without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});
		it('renders correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
		it('executes action on ok', () => {
			wrapper.find('#modal-button-Ok').simulate('submit');
			expect(executeSpy.withArgs(data, 'delete').calledOnce).toBe(true);
		});
		it('closes on cancel', () => {
			wrapper.find('#modal-button-Cancel').simulate('click');
			expect(closeSpy.calledOnce).toBe(true);
		});
	});
	describe('renders empty when not visible', () => {
		beforeEach(() => {
			closeSpy = spy();
			executeSpy = spy();
			wrapper = mount(
				<AreYouSureModal
					id="areYouSureModal" data={data} commandId="delete"
					show={false}
					closeModal={closeSpy} title="Test title" text="Test text"
					executeAction={executeSpy}
				/>);
		});

		it('without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});
		it('renders correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
	});
});
