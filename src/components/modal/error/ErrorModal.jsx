// @flow

import React from 'react';
import * as PropTypes from 'prop-types';
import type {ModalButton} from "../base/Modal";
import Modal from "../base/Modal";

type Props = {
  closeModal: (Object, string, string) => void,
  title: string,
  text: string,
  message: string,
  show: boolean,
}

export default function ErrorModal(props: Props) {

  const buttons: Array<ModalButton> = [
    {
      isSubmit: true, onClick: () => {
        props.closeModal({}, '', 'error');
      }, text: 'Ok'
    }
  ];

  return (
    <Modal show={props.show} buttons={buttons}>
      <div className="modal-header alert alert-danger">
        <h3>
          <strong>{props.title}</strong>
        </h3>
      </div>
      <div className="modal-body">
        <div><strong>{props.text}</strong></div>
        <div>Message: {props.message}</div>
      </div>
    </Modal>
  );
}
ErrorModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  show: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

