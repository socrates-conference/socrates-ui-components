import React from 'react';
import {mount} from 'enzyme';
import {spy} from 'sinon';
import ErrorModal from '.';

describe('(Component) ErrorModal', () => {
  let wrapper;
  let closeSpy;
	describe('renders content when visible', () => {
		beforeEach(() => {
			closeSpy = spy();
			wrapper = mount(
				<ErrorModal
					show={true} closeModal={closeSpy}
					title="Test Title" text="Test text" message="Error"
				/>
			);
		});

		it('without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});
		it('renders correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
		it('closes on ok', () => {
			wrapper.find('#modal-button-Ok').simulate('submit');
			expect(closeSpy.calledOnce).toBe(true);
		});
	});
	describe('renders empty when not visible', () => {
		beforeEach(() => {
			closeSpy = spy();
			wrapper = mount(
				<ErrorModal
					show={false} closeModal={closeSpy}
					title="Test Title" text="Test text" message="Error"
				/>
			);
		});

		it('without exploding', () => {
			expect(wrapper).toHaveLength(1);
		});
		it('renders correctly', () => {
			expect(wrapper).toMatchSnapshot();
		});
	});
});
